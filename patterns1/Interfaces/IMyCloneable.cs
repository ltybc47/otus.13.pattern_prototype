﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Channels;

namespace Pattern_Prototype
{
  interface IMyCloneable<T>
  {
    /// <summary>
    /// Клон объекта
    /// </summary>
    /// <returns></returns>
    T MyClone() ;
  }
}
