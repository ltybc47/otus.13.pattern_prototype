﻿1. Добрый день, ДЗ на порождающие паттерны

Реализуем паттерн "Прототип"
Цель: Создаст иерархию из нескольких классов, в которых реализованы методы клонирования объектов по шаблону проектирования "Прототип".
0. Придумать и создать 3-4 класса, которые как минимум дважды наследуются и написать краткое описание текстом.
-Pin - пин, базовый класс описывающий состояние одного из параметров объекта (ножка микросхемы)
--BoolPin
--RealPin
--IntPin
-Node - Нода, отдельный самодостаточный элемент модели(микросхема), имеет входные и выходные параметры отображаемые через пины 
-Temperature
--Manometr
--ventilator
-Model - Модель, состоит из нодов, ноды состоят из входных и выходных пинов. (печатная плата состоит из микросхем, состоящая из ножек)
SmartElectroMotor
1. Создать свой интерфейс IMyCloneable для реализации шаблона "Прототип".
+

2. Сделать возможность клонирования объекта для каждого из этих классов, используя вызовы родительских конструкторов.
+
Тут сделал 2 варианта, для класса Node функция клонирования реализованна в конструкторе Node(Node node)
Для класса Model клонирование реализованно в конструкторе Model(Model model), который эту функцию делегирует абстрактному классу.
Вот, какой вариант лучше непонятно, скорее зависит от ситуации.

3. Составить тесты или написать программу для демонстрации функции клонирования.
1. RealPinTests
2. 
4. Добавить к каждому классу реализацию стандартного интерфейса ICloneable и реализовать его функционал через уже созданные методы.
+
5. Написать вывод: какие преимущества и недостатки у каждого из интерфейсов: IMyCloneable и ICloneable.
ICloneable  - возвращает object, не всегда удобно, нужно приводить типы. Но стандартный интерфейс .Net
IMyCloneable - можно параметрировать выходной тип у интерфейса, можно через параметр настроить полный или частичный клон нам нужен


Критерии оценки: +1 байт - есть краткое описание созданных классов
+3 байта - реализован шаблон проектирования Prototype с пользовательским интерфейсом.
+3 байта - реализован шаблон проектирования Prototype со стандартным интерфейсом.
+1 байта - созданы тесты/написано тестирование функционала.
+2 байта - написан вывод о преимуществах и недостатках каждого метода.
