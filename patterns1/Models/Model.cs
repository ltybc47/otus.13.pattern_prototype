﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pattern_Prototype
{
  /// <summary>
  /// Абстрактная модель, можем представить микросхему с ножками входными и выходными
  /// </summary>
  public abstract class Model : ICloneable, IMyCloneable<Model>
    //: ICloneable
  {
    public string Description { get; set; }
    public string Name { get; private set; }

    public Dictionary<string, Pin> InputPins = new Dictionary<string, Pin>();
    public Dictionary<string, Pin> OutputPins = new Dictionary<string, Pin>();
    
    public Dictionary<string, Node> Nodes = new Dictionary<string, Node>();

    //public ILogger Logger;

    public Model(string name, string description)
    {
      Name = name;
      Description = description;
      InitPins();
      InitNodes();
      InitLogic();
    }
    /// <summary>
    /// конструктор прототипа 
    /// </summary>
    /// <param name="model"></param>
    public Model( Model model)
    {
      this.Name = model.Name;
      this.Description = model.Description;
      // вроде как можно сделать таким способом, но мы склонируем все подобъекты через интерфейс
      if (false)
      {
        // Возможно тут скопируются привязки на события но не проверял
        this.InputPins = new Dictionary<string, Pin>(model.InputPins);
        this.OutputPins= new Dictionary<string, Pin>(model.OutputPins);
        this.Nodes = new Dictionary<string, Node>(model.Nodes);
      }
      else
      {
        InputPins.Clear();
        OutputPins.Clear();
        Nodes.Clear();

        foreach (KeyValuePair<string, Pin> pin in model.InputPins)
        {
          InputPins.Add(pin.Key, pin.Value.MyClone());
        }
        foreach (KeyValuePair<string, Pin> pin in model.OutputPins)
        {
          OutputPins.Add(pin.Key, pin.Value.MyClone());
        }        
        foreach (KeyValuePair<string, Node> pin in model.Nodes)
        {
          Nodes.Add(pin.Key, pin.Value.MyClone());
        }

      }
      InitLogic();
    }

    public abstract void StartLogic();
    internal abstract void InitPins();
    internal abstract void InitNodes();
    internal abstract void InitLogic();
    public abstract object Clone();
    public abstract Model MyClone();
    public override string ToString()
    {
      return $"Type - {this.GetType().Name}; Name = {Name}, Description {Description} \n" + 
        string.Join("\n", Nodes.Select(x => x.ToString()));
    }
    ///// <summary>
    ///// Клонирует объект, минусом является что возвращается Object, и мы не можем привести к типу наследника.
    ///// </summary>
    ///// <returns></returns>
    public void baseClone()
    {
    }

  }
}
