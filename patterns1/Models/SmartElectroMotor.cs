﻿using Pattern_Prototype.Nodes;
using Pattern_Prototype.Pins;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pattern_Prototype.Models
{
  /// <summary>
  /// Электромотор
  /// </summary>
  public class SmartElectroMotor : Model
  {
    public SmartElectroMotor(string name, string desc) : base(name, desc)
    {
    }
    public SmartElectroMotor(SmartElectroMotor model) : base(model)
    {
      //foreach(KeyValuePair<string, Pin> p in model.InputPins)
      //{
      //  InputPins.Add(p.Key, p.Value.MyClone());
      //}
    }

    internal override void InitPins()
    {
      InputPins.Add("T", new RealPin("Температура окружающей среды", 20));
      InputPins.Add("btnStart", new BoolPin("Start M1"));
      InputPins.Add("btnStop", new BoolPin("Stop M1"));
      InputPins.Add("btnCooling", new BoolPin("Start Cooling"));

    }

    internal override void InitNodes()
    {
      //добавили устройства
      Nodes.Add("T1", new TemperatureSensor("T1", "Датчик такой то"));
      Nodes.Add("M", new ElectroMotor("Motor1", "Мотор такой то "));
      Nodes.Add("V", new Ventilator("Ventilator1", "Вентилятор такой то"));     
    }

    internal override void InitLogic()
    {
      // обвязка устройств

      InputPins["T"].PropertyChanged += (sender, e) => {
        // Датчик температуры измеряет температуру окружающей среды 
        Nodes["T1"].InputPins["now"].SetValue(InputPins["T"].GetValue());
      };
      // Вешаем логику на датчик высокой температуры 
      Nodes["T1"].OutputPins["flagHi"].PropertyChanged += (sender, e) => {
        if (Nodes["T1"].OutputPins["flagHi"].GetValue())
        {
          Nodes["V"].InputPins["BtnStart"].SetValue(true);      // включение вентилятора
          Nodes["M"].InputPins["BtnStart"].SetValue(false);      // отключение мотора
        }
      };
      // Вешаем логику на датчик низкой температуры 
      Nodes["T1"].OutputPins["flagLo"].PropertyChanged += (sender, e) => {
        if (Nodes["T1"].OutputPins["flagLo"].GetValue())
        {
          Nodes["V"].InputPins["BtnStart"].SetValue(false);     //  отключение вентилятора 
        }
      };
    }

    public override void StartLogic()
    {
      // Если движок запустился 
      if (InputPins["BtnStart"].GetValue())
      {
        // симитировали повышение температуры при включенном двигателе
        Nodes["T1"].InputPins["now"].SetValue(
          Nodes["T1"].InputPins["now"].GetValue() + 1.0
          );

        Console.WriteLine("Motor is work");
      }
      else
      {
        Console.WriteLine("Motor dosen't work");
      }
    }

    public override Model MyClone()
    {
      return new SmartElectroMotor(this);
    }

    public override object Clone()
    {
      return MyClone();
    }
  }
}
