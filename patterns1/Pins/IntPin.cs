﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pattern_Prototype.Pins
{
  /// <summary>
  /// Целочисленный пин
  /// </summary>
  class IntPin : Pin
  {
    private int value;

    public IntPin(string name) : base(name)
    {
    }

    public IntPin(IntPin pin): base(pin.Name)
    {
      base.Description = pin.Description;
      SetValue(pin.GetValue());
    }

    public override Pin MyClone()
    {
      return new IntPin(this) { value = this.value };
    }

    public override dynamic GetValue()
    {
      return value;
    }

    public override void SetValue(dynamic val)
    {
      if (value != (int)val)
      {
        value = (int)val;
        OnPropertyChanged();
      }
    }

    public override object Clone()
    {
      return MyClone();
    }
  }
}
