﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Pattern_Prototype.Pins
{
  /// <summary>
  /// Булейвый пин, отвечает а ввод или вывод булевого сигнала
  /// </summary>
  public class BoolPin : Pin
  {
    private bool value;

    public override dynamic GetValue()
    {
      return value;
    }

    public override void SetValue(dynamic val)
    {
      if (value != (bool)val)
      {
        value = (bool)val;
        OnPropertyChanged();
      }
    }

    public BoolPin(string name) : base(name)
    {
    }

    public BoolPin(BoolPin pin): base(pin.Name)
    {
      base.Description = pin.Description;
      SetValue(pin.GetValue());
    }

    public event PropertyChangedEventHandler PropertyChanged;

    public override Pin MyClone()
    {
      return new BoolPin(this) { value = this.value };
    }

    public override object Clone()
    {
      return MyClone();
    }
  }
}
