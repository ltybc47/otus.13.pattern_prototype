﻿using System;
using System.Security.Cryptography;

namespace Pattern_Prototype
{
  /// <summary>
  /// Аналоговый пин, отвечает а ввод или вывод аналогового сигнала
  /// </summary>
  public class RealPin : Pin
  {
    private double value;

    public override dynamic GetValue()
    {
      return value;
    }
    public override void SetValue(dynamic val)
    {
      value = (double)val;
      OnPropertyChanged();
    }

    public RealPin(string name) : base(name)
    {
    }
    public RealPin(string name, double val) : base(name)
    {
      value = val;
    }

    public RealPin(RealPin pin) : base(pin.Name)
    {
      base.Description = pin.Description;
      SetValue(pin.GetValue());
    }

    public override Pin MyClone()
    {
      return new RealPin(this) { value = this.value };
    }

    public override object Clone()
    {
      return MyClone();
    }
  }
}