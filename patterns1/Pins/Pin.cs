﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace Pattern_Prototype
{
  /// <summary>
  /// Пин, можно сказать что это ножка микросхемы
  /// </summary>
  public abstract class Pin : INotifyPropertyChanged, ICloneable, IMyCloneable<Pin>
  {
    public string Name { get; set; }
    public string Description { get; set; }
    public Pin( string name)
    {
      Name = name;
    }
    public Node MasterModel { get; set; }

    public event PropertyChangedEventHandler PropertyChanged;

    public void OnPropertyChanged([CallerMemberName] string nameMethod = "")
    {
      if (PropertyChanged != null)
        PropertyChanged(this, new PropertyChangedEventArgs(nameMethod));
    }

    public abstract void SetValue(dynamic val);
    public abstract dynamic GetValue(); 
    public abstract object Clone();
    public abstract Pin MyClone();
    public override string ToString()
    {
      return $"{this.GetType().Name} - Name - {Name}; Description - {Description}";
    }

  }
}
