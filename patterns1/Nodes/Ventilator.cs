﻿using Pattern_Prototype.Pins;
using System;

namespace Pattern_Prototype.Nodes
{
  class Ventilator : Node
  {
    public Ventilator(string name, string descript) : base(name, descript)
    {
    }
    public Ventilator(Ventilator ventilator) : base(ventilator)
    {
    }

    public override object Clone()
    {
      return MyClone();
    }

    public override Node MyClone()
    {
      return new Ventilator(this);
    }

    public override void StartLogic()
    {
    }

    internal override void InitLogic()
    {
    }

    internal override void InitPins()
    {

      InputPins.Add("BtnStart", new BoolPin("Start"));
      InputPins.Add("cPw", new RealPin("Cooling Power", 5.0));
    }
  }
}
