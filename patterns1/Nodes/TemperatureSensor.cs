﻿using Pattern_Prototype.Pins;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pattern_Prototype.Nodes
{
  public class TemperatureSensor : Node
  {
    /// <summary>
    /// Температурный датчик
    /// </summary>
    /// <param name="name"></param>
    public TemperatureSensor(string name, string descript) : base(name, descript)
    {
    }
    public TemperatureSensor(TemperatureSensor temperatureSensor) : base(temperatureSensor)
    {
    }

    internal override void InitPins()
    {
      InputPins.Add("min", new RealPin("Мин", 0.0) { Description = "Минимум", });
      InputPins.Add("max", new RealPin("Макс", 100.0) { Description = "Максимум", });
      InputPins.Add("now", new RealPin("Текущая", 25.0) { Description = "Текущая температура", });
      InputPins.Add("hide", new RealPin("Авар.", 50.0) { Description = "Уставка сработки датчика", });
      InputPins.Add("low", new RealPin("Авар.", 10.0) { Description = "Уставка сработки датчика", });

      OutputPins.Add("flagHi", new BoolPin("Алярм! Повышенная температура"));
      OutputPins.Add("flagLo", new BoolPin("Алярм! Пониженная температура"));
    }
    internal override void InitLogic()
    {

      //логика сработки датчика при превышении темпаратуры выше уставки
      InputPins["now"].PropertyChanged += (sender, e) => {

        OutputPins["flagHi"].SetValue(InputPins["now"].GetValue() > InputPins["hide"].GetValue());
        OutputPins["flagLo"].SetValue(InputPins["now"].GetValue() < InputPins["low"].GetValue());

      };
    }

    public override void StartLogic()
    {
      
    }

    public override Node MyClone()
    {
      return new TemperatureSensor(this);
    }

    public override object Clone()
    {
      return MyClone();
    }
  }
}
