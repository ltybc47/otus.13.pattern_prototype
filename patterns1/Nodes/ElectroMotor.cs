﻿using Pattern_Prototype.Pins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;

namespace Pattern_Prototype.Nodes
{
  class ElectroMotor : Node
  {

    public ElectroMotor(string name, string desc) : base(name, desc)
    {
    }

    public ElectroMotor(ElectroMotor electroMotor) : base(electroMotor)
    {
    }

    public override object Clone()
    {
      return MyClone();
    }

    public override Node MyClone()
    {
      // клонируем
      ElectroMotor newM = new ElectroMotor(this);
      return newM;
    }

    public override void StartLogic()
    {
      throw new NotImplementedException();
    }

    internal override void InitLogic()
    {
    }

    internal override void InitPins()
    {
      InputPins.Add("BtnStart", new BoolPin("Start"));
      OutputPins.Add("ErrorFlag", new BoolPin("Cant Work"));
    }
  }
}
