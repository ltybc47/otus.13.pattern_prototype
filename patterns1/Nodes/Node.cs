﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pattern_Prototype
{
  /// <summary>
  /// Нода, отдельный самодостаточный элемент модели
  /// </summary>
  public abstract class Node: IMyCloneable<Node>
  {
    public string Name { get; set; }
    public string Description { get; set; }
    public Dictionary<string,Pin> InputPins = new Dictionary<string, Pin>();
    public Dictionary<string,Pin> OutputPins = new Dictionary<string, Pin>();
    public Node(string name, string description)
    {
      Name = name;
      Description = description;
      InitPins();
      InitLogic();
    }
    public Node(Node node)
    {
      Name = node.Name;
      Description = node.Description;
      // вроде как можно сделать таким способом, но мы склонируем все подобъекты через интерфейс
      if (false)
      {
        // Возможно тут скопируются привязки на события но не проверял
        this.InputPins = new Dictionary<string, Pin>(node.InputPins);
        this.OutputPins = new Dictionary<string, Pin>(node.OutputPins);
      }
      else
      {
        InputPins.Clear();
        OutputPins.Clear();
        foreach (KeyValuePair<string, Pin> pin in node.InputPins)
        {
          InputPins.Add(pin.Key, pin.Value.MyClone());
        }
        foreach (KeyValuePair<string, Pin> pin in node.OutputPins)
        {
          OutputPins.Add(pin.Key, pin.Value.MyClone());
        }
      }
      InitLogic();
    }

    public abstract void StartLogic();
    internal abstract void InitPins();
    internal abstract void InitLogic();
    public abstract object Clone();

    public override string ToString()
    {
      return $"Name - {Name}; Description - {Description} \n" +
        string.Join("\n", InputPins.Select(x => x.Value.ToString() + " " + x.Value.GetValue())) + 
        string.Join("\n", OutputPins.Select(x => x.Value.ToString() + " " + x.Value.GetValue()));
    }

    public abstract Node MyClone();
  }
}
