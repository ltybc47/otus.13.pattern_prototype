﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pattern_Prototype;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pattern_Prototype.Tests
{
  [TestClass()]
  public class RealPinTests
  {
    [TestMethod()]
    public void RealPinTest()
    {

      try
      {
        RealPin pin = new RealPin("test", 123.321);

        //Когда нам нужно клонировать объект через представление базовоно класса то клонируем через метод
        //Нужно когда в один список базового класса вкладываются несколько различных классов наследников.
        RealPin pin2 = (RealPin)pin.MyClone();

        // Когда нам нужен точный тип мы возвращаем клона через констуктор класса этого типа
        RealPin pin3 = new RealPin(pin2);

        // почему то не вывожится в результат вывода теста, но значение одинаковые 
        Console.WriteLine(pin);
        Console.WriteLine(pin2);
        Console.WriteLine(pin3);


      }
      catch (Exception e)
      {

        Assert.Fail(e.Message + " \n" + e.Source);
      }
    }
  }
}