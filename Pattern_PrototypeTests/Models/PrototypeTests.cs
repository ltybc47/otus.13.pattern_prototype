﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pattern_Prototype.Models;
using Pattern_Prototype.Nodes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pattern_Prototype.Models.Tests
{
  [TestClass()]
  public class PrototypeTests
  {
    [TestMethod()]
    public void ICloneableTest()
    {
      try
      {
        SmartElectroMotor M1 = new SmartElectroMotor("Smart Motor", " ");
        Console.WriteLine(M1.ToString());

        var M3 = M1.MyClone();
        Console.WriteLine("-------------");
        Console.WriteLine(M3.ToString());

        Console.WriteLine($"M1 Одинаковые ли типы у исходинка и клона?  {M1.GetType().Name == M3.GetType().Name}");

      }
      catch (Exception e)
      {
        Assert.Fail(e.Message + "\n" + e.Source);
      }
    }
  }
}